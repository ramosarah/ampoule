'use strict';


console.log('Hello world!');


function toggle(mac) {
    fetch('http://192.168.1.62:8080/api/v1/devices/' + mac,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            action : 'toggle',
            color : '00FFFFFF'
        })
    }
    )
}

function color(mac, color) {
    fetch('http://192.168.1.62:8080/api/v1/devices/' + mac,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            color : color,
            ramp : 10

        })
    }
    )
}

function colorFromInput(mac){
    // Je recupere l'input de type color
    let inputColor = document.querySelector('#newColor');
    // Je recupere la value de cet input ex : #1fed17
    let color = inputColor.value;
    // J'enleve le # ex : 1FED17
    color = color.substring(1).toUpperCase();
    // Je rajoute les deux 0 devant : 001FED17
    color = '00'+color;
    console.log("ColorFomInput : "+color);


    fetch('http://192.168.1.62:8080/api/v1/devices/' + mac,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            color : color
        })
    }
    )
}



function setNewIntensity(mac, intensity){
    // Je recupere l'input de type color
    //let inputRange = document.querySelector('#newIntensity');
    // Je recupere la value de cet input ex : #1fed17
    //let intensity = inputRange.value;
    // J'enleve le # ex : 1FED17
    //console.log(" "+color);


    fetch('http://192.168.1.62:8080/api/v1/devices/' + mac,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            color : '8;' + intensity
        })
    }
    )
}


function colorSwitch(mac, counter) {
    console.log("counter="+counter);
    let colorGoal;
    switch(counter%2) {
        case 1:
            colorGoal = '00FF0000';
            break;

        case 0 :
            colorGoal = '00ffff00';
            break;

        default:
    }

    fetch('http://192.168.1.62:8080/api/v1/devices/' + mac,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            ramp : 10,
            color : colorGoal

        })
    }
    )
}



function onoff(mac, on){
    let currentvalue = document.getElementById(mac).value;
    if(currentvalue == "off"){
      document.getElementById(mac).value="on";
      return "on";
    }else{
      document.getElementById(mac).value="off";
      return "off";
    }
    fetch('http://192.168.1.62:8080/api/v1/devices/' + mac,
    {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            action : actionOnOff
        })
    }
    )


}



fetch('http://192.168.1.62:8080/api/v1/devices')
    .then(resp => resp.json())
    .then(httpBody => {
        const bulbList = document.querySelector('#bulbs');
        //const bulbColor = document.querySelector('#bulbs');
        for (let bulb of httpBody) {
            let htmlBulbList = document.createElement('p');
            htmlBulbList.innerText = bulb.mac;
            bulbList.appendChild(htmlBulbList);

            let intensity = document.createElement('input');
            intensity.type = 'range';
            intensity.min = 0;
            intensity.max = 100;
            bulbList.id = bulb.mac;
            bulbList.appendChild(intensity);
            intensity.addEventListener('input', () => setNewIntensity(bulb.mac, intensity.value));

            /*let actionOnOff = document.createElement('button');
            actionOnOff.innerText = '';
            actionOnOff.id = bulb.mac;
            bulbList.appendChild(actionOnOff);
            actionOnOff.addEventListener('click', () => onoff(bulb.mac));
            actionOnOff.value = onoff(bulb.mac);*/

            let onoffBulb = document.createElement('input');
            onoffBulb.type= 'button';
            onoffBulb.addEventListener('click', () => toggle(bulb.mac));
            onoffBulb.addEventListener('click', () => onoff(bulb.mac));
            onoffBulb.id = bulb.mac;
            bulbList.appendChild(onoffBulb);
            onoffBulb.value = onoff(bulb.mac);

            let htmlBulbColor = document.createElement('button');
            htmlBulbColor.innerText = 'CHANGE ';
            bulbList.appendChild(htmlBulbColor);
            htmlBulbColor.addEventListener('click', () => color(bulb.mac, '00196AF6'))

            let htmlBulbInput = document.createElement('button');
            htmlBulbInput.innerText = 'INPUT ';
            bulbList.appendChild(htmlBulbInput);
            htmlBulbInput.addEventListener('click', () => colorFromInput(bulb.mac))

            let countClick = 0;
            let htmlBulbColorSwith = document.createElement('button');
            htmlBulbColorSwith.innerText = 'SWITCH ';
            bulbList.appendChild(htmlBulbColorSwith);
            htmlBulbColorSwith.addEventListener('click', () => colorSwitch(bulb.mac, countClick++))

            let htmlBulbColorStrombo = document.createElement('button');
            htmlBulbColorStrombo.innerText = 'STROMBO ';
            bulbList.appendChild(htmlBulbColorStrombo);
            htmlBulbColorStrombo.addEventListener('click', () => color(bulb.mac, '00008000'))
            htmlBulbColorStrombo.addEventListener('click', () => color(bulb.mac, '00800080'))

            /*let htmlBulbOnOff = document.createElement('button');
            htmlBulbOnOff.innerText = (bulb.mac).value;
            bulbList.appendChild(htmlBulbOnOff);
            htmlBulbOnOff.addEventListener('click', () => onoff(bulb.mac).value)*/
        }
    });



/* TOGGLE
let htmlBulb = document.createElement('button');
htmlBulb.innerText = 'TOGGLE';
bulbList.appendChild(htmlBulb);
htmlBulb.addEventListener('click', () => toggle(bulb.mac))
*/





/*
fetch('http://192.168.1.62:8080/api/v1/devices')
    .then(resp => resp.json())
    .then(httpBody => {
        const bulbList = document.querySelector('#bulbs');
        for (let bulb of httpBody) {
            let htmlBulbList = document.createElement('span');
            htmlBulbList.innerText = 'MAC : ' + bulb.mac;
            bulbList.appendChild(htmlBulbList);
        }
    });
*/

/*
//CHANGE
fetch('http://192.168.1.62:8080/api/v1/devices')
    .then(resp => resp.json())
    .then(httpBody => {
        const bulbColor = document.querySelector('#bulbs');
        for (let bulb of httpBody) {
            let htmlBulbColor = document.createElement('button');
            htmlBulbColor.innerText = 'CHANGE : ' + bulb.mac;
            bulbColor.appendChild(htmlBulbColor);
            htmlBulbColor.addEventListener('click', () => color(bulb.mac, '00196AF6'))
        }
    });


//INPUT
fetch('http://192.168.1.62:8080/api/v1/devices')
    .then(resp => resp.json())
    .then(httpBody => {
        const bulbList = document.querySelector('#bulbs');
        for (let bulb of httpBody) {
            let htmlBulb = document.createElement('button');
            htmlBulb.innerText = 'INPUT  : ' + bulb.mac;
            bulbList.appendChild(htmlBulb);
            htmlBulb.addEventListener('click', () => colorFromInput(bulb.mac))
        }
    });


//SWITCH
let countClick = 0; //variable grobale à la page
fetch('http://192.168.1.62:8080/api/v1/devices')
    .then(resp => resp.json())
    .then(httpBody => {
        const bulbColor = document.querySelector('#bulbs');
        for (let bulb of httpBody) {
            let htmlBulbColor = document.createElement('button');
            htmlBulbColor.innerText = 'SWITCH : ' + bulb.mac;
            bulbColor.appendChild(htmlBulbColor);
            htmlBulbColor.addEventListener('click', () => colorSwitch(bulb.mac, countClick++))
        }
    });

//STROMBO avec fct color()
fetch('http://192.168.1.62:8080/api/v1/devices')
    .then(resp => resp.json())
    .then(httpBody => {
        const bulbColor = document.querySelector('#bulbs');
        for (let bulb of httpBody) {
            let htmlBulbColor = document.createElement('button');
            htmlBulbColor.innerText = 'STROMBO : ' + bulb.mac;
            bulbColor.appendChild(htmlBulbColor);
            htmlBulbColor.addEventListener('click', () => color(bulb.mac, '00008000'))
            htmlBulbColor.addEventListener('click', () => color(bulb.mac, '00800080'))

        }
    });


*/
